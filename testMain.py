import sys

import programFunkcje
# z bazy
sys.stdin = open("test.txt")
programFunkcje.run()
# z pliku
sys.stdin = open("test2.txt")
programFunkcje.run()
# ręcznie
sys.stdin = open("test3.txt")
programFunkcje.run()

sys.exit()

