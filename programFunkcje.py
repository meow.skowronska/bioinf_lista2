import numpy as np


class SeqData:
    def __init__(self, description, seq):
        self.description = description
        self.seq = seq


# podaj gi or accesion
def getSeqDataFromNcbi(gi):
    from Bio import Entrez
    from Bio import SeqIO
    Entrez.email = "A.N.Other@example.com"
    with Entrez.efetch(
            db="nuccore", rettype="fasta", retmode="text", id=gi
    ) as handle:
        seq_record = SeqIO.read(handle, "fasta")
    seqData = SeqData(seq_record.description, seq_record.seq)
    return seqData


# 1393630358
# 1400687864
# print(getSeqDataFromNcbi("1393630358","nuccore").description)

# z konsoli
def getSeqDataFromInput(description, seq):
    return SeqData(description, seq)


# z pliku
def getSeqDataFromFile(filename):
    with open(filename, "r") as myfile:
        Lines = myfile.readlines()
        line = Lines[0]
    if line.startswith('>'):
        seqData = SeqData("", "")
        seqData.description = line[1:len(line)]
        seqData.seq = "".join(Lines[1:len(Lines)]).strip()
    return seqData


def wypelnijTablicePunktow(mode, match, mismatch, gap, seq1, seq2):
    arr = np.zeros([len(seq1) + 1, len(seq2) + 1])
    ar2 = arr[0:len(seq1) + 1, 0]
    for index, x in np.ndenumerate(ar2):
        arr[index[0], 0] = x + gap * index[0]
    ar2 = arr[0, 0:len(seq2) + 1]
    for index, x in np.ndenumerate(ar2):
        arr[0, index[0]] = x + gap * index[0]
    for index, x in np.ndenumerate(arr[1:arr.shape[0], 1:arr.shape[1]]):
        if seq1[index[0]] == seq2[index[1]]:
            matchScore = match + arr[index[0], index[1]]
        else:
            matchScore = mismatch + arr[index[0], index[1]]
        gapIns = gap + arr[index[0], index[1] + 1]
        gapDel = gap + arr[index[0] + 1, index[1]]
        if mode == "similarity":
            arr[index[0] + 1, index[1] + 1] = max(matchScore, gapIns, gapDel)
        elif mode == "distance":
            arr[index[0] + 1, index[1] + 1] = min(matchScore, gapIns, gapDel)

    return arr


def getSciezkaDopasowania(seq1, seq2, F, match, mismatch, gap):
    aln1 = ""
    aln2 = ""
    i = len(seq1)
    j = len(seq2)
    seq1 = "-" + seq1
    seq2 = "-" + seq2
    score = 0
    matches = 0
    gaps = 0
    sciezka = np.zeros([len(seq1), len(seq2)])
    while i > 0 or j > 0:
        if i > 0 and j > 0 and F[i, j] == F[i - 1, j - 1] + mismatch * (seq1[i] != seq2[j]):
            aln1 = seq1[i] + aln1
            aln2 = seq2[j] + aln2
            sciezka[i, j] = 1
            i = i - 1
            j = j - 1
            score = score + mismatch
        elif i > 0 and j > 0 and F[i, j] == F[i - 1, j - 1] + match * (seq1[i] == seq2[j]):
            aln1 = seq1[i] + aln1
            aln2 = seq2[j] + aln2
            sciezka[i, j] = 1
            matches = matches + 1
            i = i - 1
            j = j - 1
            score = score + match
        elif i > 0 and F[i, j] == F[i - 1, j] + gap:
            aln1 = seq1[i] + aln1
            aln2 = "-" + aln2
            sciezka[i, j] = 1
            gaps = gaps + 1
            i = i - 1
            score = score + gap
        else:
            aln1 = "-" + aln1
            aln2 = seq2[j] + aln2
            sciezka[i, j] = 1
            gaps = gaps + 1
            j = j - 1
            score = score + gap
    sciezka[i, j] = 1
    return sciezka, score, matches, gaps, aln1, aln2


def split(word):
    return [char for char in word]


def run():
    sposob = input("Podaj sposób wczytywania: 1 ręcznie, 2 z pliku, 3 z bazy ncbi\n")
    if sposob == "1":
        print("Podaj nazwę pierwszej sekwencji:\n")
        print("Podaj pierwszą sekwencje:\n")
        seq1Data = getSeqDataFromInput(input(), input())
        print("Podaj nazwę drugiej sekwencji:\n")
        print("Podaj drugą sekwencje:\n")
        seq2Data = getSeqDataFromInput(input(), input())
    elif sposob == "2":
        seq1Data = getSeqDataFromFile(input("Podaj nazwę pliku pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromFile(input("Podaj nazwę pliku drugiej sekwencji\n"))
    elif sposob == "3":
        seq1Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION drugiej sekwencji\n"))
    mode = input("Wybierz sposob punktowania 1 distance, 2 similarity\n")
    match = int(input("Punkty za match:\n"))
    mismatch = int(input("Punkty za mismatch:\n"))
    gap = int(input("Punkty za gap:\n"))
    if mode == "1":
        mode = "distance"
    elif mode == "2":
        mode = "similarity"

    F = wypelnijTablicePunktow(mode, match, mismatch, gap, seq1Data.seq, seq2Data.seq)
    sciezka, score, matches, gaps, aln1, aln2 = getSciezkaDopasowania(seq1Data.seq, seq2Data.seq, F, match, mismatch,
                                                                      gap)
    import matplotlib.pyplot as plt
    import seaborn as sns

    if F.size>1000000:
        print(F[0:1000, 0:1000])
        yticks = split("-" + seq1Data.seq[0:999])
        xticks = split("-" + seq2Data.seq[0:999])
        plt.figure(figsize=(100, 100), dpi=100)

        ax = sns.heatmap(F[0:1000, 0:1000], mask=sciezka[0:1000, 0:1000], yticklabels=yticks, xticklabels=xticks,
                         cbar=False, square=True, linewidths=0.5)
        print("Wyświetlanie fragmentu tablicy, sekwencje za długie")

    else:

        yticks = split("-" + seq1Data.seq)
        xticks = split("-" + seq2Data.seq)
        plt.figure(figsize=(50, 50), dpi=100)

        ax = sns.heatmap(F, mask=sciezka, yticklabels=yticks, xticklabels=xticks, cbar=False, square=True)

    ax.xaxis.set_ticks_position('top')
    fig = ax.get_figure()
    fig.savefig(input("Podaj nazwę pliku do zapisania graficznej tablicy punktacji:\n"))
    plt.show()
    f = open(input("Podaj nazwę pliku do zapisania statystyk"), "w+")
    length = str(len(aln1))
    identity = str(matches) + "/" + str(len(aln1))
    gaps = str(gaps) + "/" + str(len(aln1))
    alingment = ""
    for x in range(0, len(aln1)):
        if aln1[x] == aln2[x]:
            alingment = alingment + "|"
        else:
            alingment = alingment + " "
    f.write(
        '# 1: {}\n# 2: {}\n# Mode: {}\n# Match: {}\n# Mismatch: {}\n# Gap: {}\n# Score: {}\n# Length: {}\n# Identity: {}\n# Gaps: {}\n{}\n{}\n{}'.format(
            seq1Data.seq, seq2Data.seq, mode, match, mismatch, gap, score, length, identity, gaps, aln1, alingment,
            aln2))
    f.close()
